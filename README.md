# Receiver Delivery Service

This is a sample project implemented using below stack:
<ul>
<li>Kotlin, as a project base language</li>
<li>Spring-Boot, as a main framework</li>
<li>GraphQl-Kotlin, provides collection of libraries to 
ease the development of GraphQL applications with
kotlin : https://expediagroup.github.io/graphql-kotlin</li>
<li>H2 database, an in-memory database which needs no configuration</li>
<li>Gradle, as our build tools and dependency manager</li>
</ul>

The project uses the schema.sql, and the data.sql files in the classpath to bootstrap database data.
To load this files in the database, I have implemented a db initializer class.

My main and only Entity is Delivery:
``` kotlin
Delivery(
val id: Long,
val product: String,
val supplier: String,
val quantity: Long,
val expectedwarehouse: String,
val delivered: Boolean
)
```

I have exposed four Graphql services over HTTP, those are:
<ul>
<li>DELIVERIES, which does not need any parameter for calling and will return
all the existing deliveries from database</li>
<li>DELIVERY, which accepts an 'id' in number format as a parameter and look up the database with than and will return it if exists</li>
<li>DELIVERED, which accepts a 'delivered' parameter in boolean format and return all the deliveries with following status</li>
<li>UPDATE-DELIVERED, which accepts an 'id' in number format and a 'delivered' parameter in boolean format
then it will query the database and if any exist, it's status will chang to the received
'delivered' parameter</li>
</ul>

After build process and starting the application,
 graphql-kotlin automatically creates the schema and the playground for us.
  Let's head up to below address and try some queries:
  
  <h5>localhost:8080/playground</h5>
  
  graphql-kotlin also generates a complete Documentation of our schema and queriesfor us that are in the right 
  panel of the playground.
  
  
   
  
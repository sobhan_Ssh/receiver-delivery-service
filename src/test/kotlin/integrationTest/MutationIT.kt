package integrationTest

import com.gorillas.DeliveryReceiverApplication
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest(classes = [DeliveryReceiverApplication::class])
@AutoConfigureWebTestClient
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MutationIT(@Autowired private val testClient: WebTestClient) {

    @Test
    fun `verify updateDelivered mutation with true value`() {
        val mutation = "updateDelivered"

        testClient.post()
            .uri(QueryIT.GRAPHQL_ENDPOINT)
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType("application", "graphql"))
            .bodyValue(
                "mutation {\n" +
                        "  updateDelivered(id:102, delivered: true) {\n" +
                        "    id,\n" +
                        "    supplier,\n" +
                        "    product,\n" +
                        "    delivered\n" +
                        "  }\n" +
                        "}"
            )
            .exchange()
            .verifyOnlyDataExists(mutation)
            .jsonPath("${QueryIT.DATA_JSON_PATH}.$mutation.id").isEqualTo(102)
            .jsonPath("${QueryIT.DATA_JSON_PATH}.$mutation.product").isEqualTo("Saiyans")
            .jsonPath("${QueryIT.DATA_JSON_PATH}.$mutation.supplier").isEqualTo("Bardock")
            .jsonPath("${QueryIT.DATA_JSON_PATH}.$mutation.delivered").isEqualTo("true")
    }

    @Test
    fun `verify updateDelivered mutation with false value`() {
        val mutation = "updateDelivered"

        testClient.post()
            .uri(QueryIT.GRAPHQL_ENDPOINT)
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType("application", "graphql"))
            .bodyValue(
                "mutation {\n" +
                        "  updateDelivered(id:102, delivered: false) {\n" +
                        "    id,\n" +
                        "    supplier,\n" +
                        "    product,\n" +
                        "    delivered\n" +
                        "  }\n" +
                        "}"
            )
            .exchange()
            .verifyOnlyDataExists(mutation)
            .jsonPath("${QueryIT.DATA_JSON_PATH}.$mutation.id").isEqualTo(102)
            .jsonPath("${QueryIT.DATA_JSON_PATH}.$mutation.product").isEqualTo("Saiyans")
            .jsonPath("${QueryIT.DATA_JSON_PATH}.$mutation.supplier").isEqualTo("Bardock")
            .jsonPath("${QueryIT.DATA_JSON_PATH}.$mutation.delivered").isEqualTo("false")
    }

    fun WebTestClient.ResponseSpec.verifyOnlyDataExists(expectedQuery: String): WebTestClient.BodyContentSpec {
        return this.expectBody()
            .jsonPath("${QueryIT.DATA_JSON_PATH}.$expectedQuery").exists()
            .jsonPath(QueryIT.ERRORS_JSON_PATH).doesNotExist()
            .jsonPath(QueryIT.EXTENSIONS_JSON_PATH).doesNotExist()
    }

}
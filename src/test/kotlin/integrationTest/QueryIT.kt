package integrationTest

import com.gorillas.DeliveryReceiverApplication
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.test.web.reactive.server.WebTestClient


@SpringBootTest(classes = [DeliveryReceiverApplication::class])
@AutoConfigureWebTestClient
@TestInstance(PER_CLASS)
class QueryIT(@Autowired private val testClient: WebTestClient) {

    companion object {
        const val GRAPHQL_ENDPOINT = "http://localhost:8080/graphql"
        const val DATA_JSON_PATH = "$.data"
        const val ERRORS_JSON_PATH = "$.errors"
        const val EXTENSIONS_JSON_PATH = "$.extensions"
    }

    @Test
    fun `verify delivery query`() {
        val query = "delivery"

        testClient.post()
            .uri(GRAPHQL_ENDPOINT)
            .accept(APPLICATION_JSON)
            .contentType(MediaType("application", "graphql"))
            .bodyValue(
                "query {\n" +
                        "  delivery(id: 102) {\n" +
                        "    id,\n" +
                        "    product,\n" +
                        "    supplier\n" +
                        "  }\n" +
                        "}"
            )
            .exchange()
            .verifyOnlyDataExists(query)
            .jsonPath("$DATA_JSON_PATH.$query.id").isEqualTo(102)
            .jsonPath("$DATA_JSON_PATH.$query.product").isEqualTo("Saiyans")
            .jsonPath("$DATA_JSON_PATH.$query.supplier").isEqualTo("Bardock")
    }


    @Test
    fun `verify delivered query with true value`() {
        val query = "delivered"

        testClient.post()
            .uri(GRAPHQL_ENDPOINT)
            .accept(APPLICATION_JSON)
            .contentType(MediaType("application", "graphql"))
            .bodyValue(
                "query {\n" +
                        "  delivered(delivered: true) {\n" +
                        "    id,\n" +
                        "    product,\n" +
                        "    supplier\n" +
                        "  }\n" +
                        "}\n"
            )
            .exchange()
            .verifyOnlyDataExists(query)
            .jsonPath("$DATA_JSON_PATH.$query").isArray
            .jsonPath("$DATA_JSON_PATH.$query").value(Matchers.hasSize<Int>(4))
    }

    @Test
    fun `verify delivered query with false value`() {
        val query = "delivered"

        testClient.post()
            .uri(GRAPHQL_ENDPOINT)
            .accept(APPLICATION_JSON)
            .contentType(MediaType("application", "graphql"))
            .bodyValue(
                "query {\n" +
                        "  delivered(delivered: false) {\n" +
                        "    id,\n" +
                        "    product,\n" +
                        "    supplier\n" +
                        "  }\n" +
                        "}\n"
            )
            .exchange()
            .verifyOnlyDataExists(query)
            .jsonPath("$DATA_JSON_PATH.$query").isArray
            .jsonPath("$DATA_JSON_PATH.$query").value(Matchers.hasSize<Int>(4))
    }

    @Test
    fun `verify deliveries query`() {
        val query = "deliveries"

        testClient.post()
            .uri(GRAPHQL_ENDPOINT)
            .accept(APPLICATION_JSON)
            .contentType(MediaType("application", "graphql"))
            .bodyValue(
                "query {\n" +
                        "  deliveries {\n" +
                        "    id,\n" +
                        "    product,\n" +
                        "    supplier\n" +
                        "  }\n" +
                        "}"
            )
            .exchange()
            .verifyOnlyDataExists(query)
            .jsonPath("$DATA_JSON_PATH.$query").isArray
            .jsonPath("$DATA_JSON_PATH.$query").value(Matchers.hasSize<Int>(8))
    }

    fun WebTestClient.ResponseSpec.verifyOnlyDataExists(expectedQuery: String): WebTestClient.BodyContentSpec {
        return this.expectBody()
            .jsonPath("$DATA_JSON_PATH.$expectedQuery").exists()
            .jsonPath(ERRORS_JSON_PATH).doesNotExist()
            .jsonPath(EXTENSIONS_JSON_PATH).doesNotExist()
    }
}
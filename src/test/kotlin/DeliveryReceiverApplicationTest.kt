import com.gorillas.DeliveryReceiverApplication
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [DeliveryReceiverApplication::class])
class DeliveryReceiverApplicationTest {

    @Test
    fun contextLoads() {
    }

}


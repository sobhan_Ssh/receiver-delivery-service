CREATE TABLE IF NOT EXISTS DELIVERY(
    id INT AUTO_INCREMENT PRIMARY KEY,
    product VARCHAR(100),
    supplier VARCHAR(100),
    quantity INT,
    expectedwarehouse VARCHAR(100),
    delivered BOOLEAN default false
);

--     expectedDate TIMESTAMP WITH TIME ZONE,

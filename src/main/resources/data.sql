TRUNCATE TABLE DELIVERY;

INSERT INTO DELIVERY (id, product, supplier, quantity, expectedwarehouse, delivered)
VALUES
(101, 'Bananas', 'JungleInc', 1000000, 'TheMoon', true),
(102, 'Saiyans', 'Bardock', 9001, 'Namek', false),
(103, 'Skull, Crystal', 'Akator', 1, 'Headquarters', true),
(104, 'Bananas', 'JungleInc', 1, 'BerlinZoo', false),
(105, 'Apples', 'ApplesToOrangesInc', 50, 'Oranges', true),
(106, 'Salad', 'HealthyFoodInc', 600, 'Headquarters', false),
(107, 'Salad', 'HealthyFoodInc', 400, 'Headquarters', true),
(108, 'Salad', 'HealthyFoodInc', 800, 'Headquarters', false);

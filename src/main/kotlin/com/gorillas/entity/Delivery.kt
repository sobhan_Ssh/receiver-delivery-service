package com.gorillas.entity

import org.springframework.data.annotation.Id

data class Delivery(
    @Id
    val id: Long,
    val product: String,
    val supplier: String,
    val quantity: Long,
    val expectedwarehouse: String,
    val delivered: Boolean
)
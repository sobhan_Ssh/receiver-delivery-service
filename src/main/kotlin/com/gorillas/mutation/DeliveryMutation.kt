package com.gorillas.mutation

import com.expediagroup.graphql.spring.operations.Mutation
import com.gorillas.entity.Delivery
import com.gorillas.service.DeliveryService
import org.springframework.stereotype.Controller

@Controller
class DeliveryMutation(private val deliveryService: DeliveryService) : Mutation {

    suspend fun updateDelivered(id: Long, delivered: Boolean): Delivery? {
        return deliveryService.updateDelivered(id, delivered)
    }
}
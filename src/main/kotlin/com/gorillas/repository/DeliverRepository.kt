package com.gorillas.repository

import com.gorillas.entity.Delivery
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux

@Repository
interface DeliverRepository : ReactiveCrudRepository<Delivery, Long>{

    fun findByDelivered(delivered: Boolean) : Flux<Delivery>
}
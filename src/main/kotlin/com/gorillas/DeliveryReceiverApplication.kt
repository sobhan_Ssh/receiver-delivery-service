package com.gorillas

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DeliveryReceiverApplication

fun main(args: Array<String>) {
    runApplication<DeliveryReceiverApplication>(*args)
}
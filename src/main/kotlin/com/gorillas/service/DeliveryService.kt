package com.gorillas.service

import com.gorillas.entity.Delivery
import com.gorillas.repository.DeliverRepository
import kotlinx.coroutines.reactive.awaitFirstOrDefault
import kotlinx.coroutines.reactive.awaitSingle
import org.springframework.stereotype.Service

@Service
class DeliveryService(private val deliveryRepository: DeliverRepository) {

    suspend fun findAll(): List<Delivery> {
        return deliveryRepository.findAll().collectList().awaitFirstOrDefault(listOf())
    }

    suspend fun findById(id: Long): Delivery? {
        return deliveryRepository.findById(id).awaitSingle()
    }

    suspend fun findByDelivered(delivered: Boolean): List<Delivery> {
        return deliveryRepository.findByDelivered(delivered).collectList().awaitFirstOrDefault(listOf())
    }

    suspend fun updateDelivered(id: Long, delivered: Boolean): Delivery? {
        var delivery: Delivery = deliveryRepository.findById(id).awaitSingle()
        delivery = Delivery(
            delivered = delivered, expectedwarehouse = delivery.expectedwarehouse,
            id = delivery.id, supplier = delivery.supplier, product = delivery.product, quantity = delivery.quantity
        )
        return deliveryRepository.save(delivery).awaitSingle()
    }
}
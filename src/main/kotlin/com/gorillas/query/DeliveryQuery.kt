package com.gorillas.query

import com.expediagroup.graphql.spring.operations.Query
import com.gorillas.entity.Delivery
import com.gorillas.service.DeliveryService
import org.springframework.stereotype.Controller

@Controller
class DeliveryQuery(private val deliveryService: DeliveryService) : Query {

    suspend fun deliveries(): List<Delivery> {
        return deliveryService.findAll()
    }

    suspend fun delivery(id: Long): Delivery? {
        return deliveryService.findById(id)
    }

    suspend fun delivered(delivered: Boolean): List<Delivery> {
        return deliveryService.findByDelivered(delivered)
    }
}